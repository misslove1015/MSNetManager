

Pod::Spec.new do |s|
  s.name             = "MSNetManager"
  s.version          = "0.1.0"
  s.summary          = "MSNetManager"
  s.description      = <<-DESC.gsub(/^\s*\|?/,'')
                       An optional longer description of XUEZHIYUN

                       | * Markdown format.
                       | * Don't worry about the indent, we strip it!
                       DESC
  s.homepage         = "https://gitlab.com/misslove1015/MSNetManager.git"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'BSD'
  s.author           = { "miss" => "13116760713@163.com" }
  s.source           = { :git => "https://gitlab.com/misslove1015/MSNetManager.git", :branch => "master" }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, '8.0'
  s.requires_arc = true
  s.public_header_files = 'Pod/**/*.h'
  s.source_files = 'Pod/**/*.{h,m}'

   s.resource_bundles = {
    'MSNetManager' => ['Pod/Assets/*.png','Pod/Assets/*.jpg']
  }
  # Uncomment following lines if ZHUserKit needs to link with some static libraries.
  # s.vendored_libraries = [
  #   'ZHUserKit/lib/a-static-library.a',
  # ]

#  s.vendored_frameworks = [
#      'ZHUserKit/Frameworks/*.framework'
#  ]

s.prefix_header_contents = <<-EOS
#ifdef __OBJC__

#import <MSFoundation/MSFoundation.h>
#import <MSUIKit/MSUIKit.h>
#import <MSRoute/MSRoute.h>
#import <MSTheme/MSTheme.h>
#import <Masonry/Masonry.h>

#endif /* __OBJC__*/
EOS

  # Uncomment following lines if ZHUserKit depends on any system framework.
#  s.frameworks = 'UIKit'

 # s.weak_framework = 'MessageUI'

  # Uncomment following lines if ZHUserKit depends on any public or private pod.
  s.dependency 'AFNetworking'
  s.dependency 'Qiniu'
  
end
