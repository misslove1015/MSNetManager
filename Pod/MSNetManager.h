//
//  MSNetManager.h
//  MSModularization
//
//  Created by 郭明亮 on 2018/9/13.
//  Copyright © 2018年 郭明亮. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MSNetManager : NSObject

+ (void)GETWithURL:(NSString *)url
          paramDic:(NSDictionary *)paramDic
           success:(void(^)(NSDictionary *dic))successBlock
           failure:(void(^)(NSError *error))failureBlock;

+ (void)POSTWithURL:(NSString *)url
           paramDic:(NSDictionary *)paramDic
            success:(void(^)(NSDictionary *dic))successBlock
            failure:(void(^)(NSError *error))failureBlock;

+ (void)downloadWithURL:(NSString *)url
                   path:(NSString *)path
               progress:(void(^)(CGFloat progress))progressBlock
                success:(void(^)(NSString *path))successBlock
                failure:(void(^)(NSError *error))failureBlock;

+ (void)uploadWithURL:(NSString *)url
                 path:(NSString *)path
             progress:(void(^)(CGFloat progress))progressBlock
              success:(void(^)(NSDictionary *dic))successBlock
              failure:(void(^)(NSError *error))failureBlock;

+ (void)cancelDownload;

+ (void)cancelUpload;

@end
