//
//  MSNetManager.m
//  MSModularization
//
//  Created by 郭明亮 on 2018/9/13.
//  Copyright © 2018年 郭明亮. All rights reserved.
//

#import "MSNetManager.h"
#import "MSHTTPSessionManager.h"
#import <Qiniu/QiniuSDK.h>

@interface MSNetManager ()

@property (nonatomic, strong) NSURLSessionDownloadTask *downloadTask;
@property (nonatomic, assign) BOOL isCancelUpload;

@end

@implementation MSNetManager

+ (instancetype)defaultManager {
    static dispatch_once_t onceToken;
    static MSNetManager *manager = nil;
    dispatch_once(&onceToken, ^{
        manager = [[MSNetManager alloc]init];
    });
    return manager;
}

+ (void)GETWithURL:(NSString *)url
          paramDic:(NSDictionary *)paramDic
           success:(void(^)(NSDictionary *dic))successBlock
           failure:(void(^)(NSError *error))failureBlock {
    
    AFHTTPSessionManager *manager = [MSHTTPSessionManager sharedManager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    manager.requestSerializer.timeoutInterval = 10;
    AFJSONResponseSerializer *response = [AFJSONResponseSerializer serializer];
    response.removesKeysWithNullValues = YES;
    manager.responseSerializer = response;
    [manager GET:url parameters:paramDic progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        if (successBlock) {
            successBlock(responseObject);
        }
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        if (failureBlock) {
            failureBlock(error);
        }
    }];
}

+ (void)POSTWithURL:(NSString *)url
           paramDic:(NSDictionary *)paramDic
            success:(void(^)(NSDictionary *dic))successBlock
            failure:(void(^)(NSError *error))failureBlock {
    
    AFHTTPSessionManager *manager = [MSHTTPSessionManager sharedManager];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.requestSerializer.timeoutInterval = 10;
    AFJSONResponseSerializer *response = [AFJSONResponseSerializer serializer];
    response.removesKeysWithNullValues = YES;
    manager.responseSerializer = response;
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:url parameters:nil error:nil];
    request.timeoutInterval = 10;
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];

    NSData *data = [NSJSONSerialization dataWithJSONObject:paramDic options:NSJSONWritingPrettyPrinted error:nil];
    [request setHTTPBody:data];
    
    [[manager dataTaskWithRequest:request uploadProgress:nil downloadProgress:nil completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        if (!error) {
            if (successBlock) {
                successBlock(responseObject);
            }
            
        }else {
            if (failureBlock) {
                failureBlock(error);
            }
        }
    }] resume];
}

+ (void)downloadWithURL:(NSString *)url
                   path:(NSString *)path
               progress:(void(^)(CGFloat progress))progressBlock
                success:(void(^)(NSString *path))successBlock
                failure:(void(^)(NSError *error))failureBlock {
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];

    [MSNetManager defaultManager].downloadTask = [manager downloadTaskWithRequest:request progress:^(NSProgress * _Nonnull downloadProgress) {
        if (progressBlock) {
            progressBlock(downloadProgress.fractionCompleted);
        }
    } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
        return [NSURL fileURLWithPath:path];
    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
        if (error) {
            NSLog(@"下载出错 %@", error.localizedDescription);
        }else {
            NSLog(@"下载完成");
            if (successBlock) {
                successBlock(path);
            }
        }
    }];
    [[MSNetManager defaultManager].downloadTask resume];
}

+ (void)cancelDownload {
    [[MSNetManager defaultManager].downloadTask cancel];
}

+ (void)uploadWithURL:(NSString *)url
                 path:(NSString *)path
             progress:(void(^)(CGFloat progress))progressBlock
              success:(void(^)(NSDictionary *dic))successBlock
              failure:(void(^)(NSError *error))failureBlock {
    [MSNetManager defaultManager].isCancelUpload = NO;
    
    QNUploadManager *upManager = [[QNUploadManager alloc] init];
    QNUploadOption *uploadOption = [[QNUploadOption alloc] initWithMime:nil progressHandler:^(NSString *key, float percent) {
        dispatch_async(dispatch_get_main_queue(), ^{
            if (progressBlock) {
                progressBlock(percent);
            }
        });
    } params:nil checkCrc:NO cancellationSignal:^BOOL{
        return [MSNetManager defaultManager].isCancelUpload;
    }];
    
    [upManager putFile:path
                   key:nil
                 token:@"token"
              complete:^(QNResponseInfo *info, NSString *key, NSDictionary *resp) {
                  if (info.ok) {
                      if (successBlock) {
                          successBlock(resp);
                      }
                  }else {
                      if (failureBlock) {
                          failureBlock(nil);
                      }
                  }
              } option:uploadOption];
}

+ (void)cancelUpload {
    [MSNetManager defaultManager].isCancelUpload = YES;
}

@end
