//
//  MSNetManager.h
//  MSModularization
//
//  Created by 郭明亮 on 2018/9/13.
//  Copyright © 2018年 郭明亮. All rights reserved.
//

#import "MSHTTPSessionManager.h"

@implementation MSHTTPSessionManager

+ (AFHTTPSessionManager *)sharedManager {
    static AFHTTPSessionManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[AFHTTPSessionManager alloc]init];
    });
    return manager;
}

@end
