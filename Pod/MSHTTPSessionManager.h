//
//  MSNetManager.h
//  MSModularization
//
//  Created by 郭明亮 on 2018/9/13.
//  Copyright © 2018年 郭明亮. All rights reserved.
//

#import <AFNetworking/AFHTTPSessionManager.h>

@interface MSHTTPSessionManager : AFHTTPSessionManager

+ (AFHTTPSessionManager *)sharedManager;

@end
